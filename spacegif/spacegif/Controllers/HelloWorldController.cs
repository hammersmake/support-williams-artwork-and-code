﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using System;
namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        // 
        // GET: /HelloWorld/

        public string Index()
        {
            return "This is my default action...";
        }

        // 
        // GET: /HelloWorld/Welcome/ 
        // Requires using System.Text.Encodings.Web;
        public string Welcome(string toptext, string bottext)
        {
            var date1 = new System.DateTime();
            return HtmlEncoder.Default.Encode($"date: {date1} {toptext} IMAGEHERE {bottext}");
        }
    }
}